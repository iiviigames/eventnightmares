GML Custom Events Engine
========================

Hey, all. This is a group of scripts I wrote for using custom events in Game Maker. The purpose of them is to allow for execution of event order in any way you see fit. For example, if you want something to occur after a collision event, you need custom events. This engine allows for that.

That said, I don't think it's quite _optimal_ , to be easy on myself. (I think these may also be outdated, I will check on that ASAP and update them if that's the case.)
