///	@func							EventSetOrder(event_array)
///	@arg	{string}	map_key	the key in MAPeventorder to change to	
///	@desc							Changes the event order to a different one stored in the map

var _switchto = argument0;

// Prevent Changing or attempting to change to an order that hasn't been created.
if !ds_map_exists(MAPeventorder, _switchto) or !ds_map_exists(MAPeventdata, _switchto)
{
	show_error("Invalid event order provided, staying on current order.", false);
}
else
{
	// Change the Current_event array to the new order.
	Current_Event = ds_map_find_value(MAPeventorder, _switchto);
	Current_Event_Data = ds_map_find_value(MAPeventdata, _switchto);
	
	// Set the iterator value to the lowest current found in the new order.
	ev = Current_Event[0];
	ev_max = array_length_1d(Current_Event) - 1 ;
	
	// Set the default event.
	EventDefault(ev)
	
	// Don't forget to reset the event order changed to false!
	event_order_changed = false;
	
	#region	Debugging.
	
	SDO();
	show_debug_message("MAPeventorder size: " + string(ds_map_size(MAPeventorder)));
	show_debug_message("MAPeventdata size: " + string(ds_map_size(MAPeventdata)));
	
	for (i = 0; i < array_length_1d(Current_Event); i++)
	{
		SD(Current_Event[i]);
	}
	#endregion
}