///	@func						EventCurrent(event_array)
///	@arg	{real}	ev_array	the current event array containing the possible events	
///	@desc						Sets the current event value or array index to use

var _events = argument0;


if !is_array(_events)
{
	_events += 1;
	current_event = _events;

}
else
{
	// Add to the current event array index
	ev += 1;
	current_event = _events[ev];
	#region	Changed Code
	//if ev == ev_max
	//{
	//	ev = 0;
	//	current_event = _eventnumber[ev];
		
	//}
	//else
	//{
	//	current_event = _eventnumber[ev];
	//}
	#endregion
}
