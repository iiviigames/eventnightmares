///	@func								EventMapsAdd(events, eventsdata, key, return_key)

///	@arg	{array}	events			the event array created by EventOrder
///	@arg	{array}	events_data	the matching data array for the events array
///	@arg	{string}	key			the key to use for storing each array in their respective dsMAPS
///	@arg	{bool}		return_key		if true, this will return the provided string to use for setting the current event.


///	@desc								Adds a new event order array to MAPeventorder
///										Adds the matching event order data to MAPeventdata

var _orderevents = argument0;
var _orderdata = argument1;
var _mapkey = argument2;
var _retkey = argument3;


if !ds_map_exists(MAPeventorder, _mapkey) and !ds_map_exists(MAPeventdata, _mapkey)
{
	MAPeventorder[? _mapkey] = _orderevents;
	MAPeventdata[? _mapkey] = _orderdata;
}
else
{
	show_error("An event order with this name already exists! Remove the old one first, or use a different name next time. Exiting.", true);
}

if _retkey
{
	return _mapkey;
}
else
{
	exit;
}

#region	Original EventListAdd functionality.
/*
var _id = argument0;
//(EV, 

var _handle = object_get_name(object_index);
show_debug_message("object_name: " + _handle);


if object_is_ancestor(_id, o_custom_events)
{
	ds_list_add(LISTevents, _handle, _array);
}
*/
#endregion