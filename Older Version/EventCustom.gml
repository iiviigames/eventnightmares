///	@func			EventCustom()

///	@desc			Creates Event Data Structures and accompanying variables.
///					2 dsMAPS	MAPeventorder & MAPeventdata

#region	Create dsMAPS for the Object's Events.
/*
No matter what, every custom event object will have all possible events.
These events will not change ever, however, their order can change.
That means that it doesn't make sense to change what the event itself is.

For example, whatever code that runs in the begin step normally  is in event user 0.
If for some reason I don't want the begin step to run, then I shouldn't move the code from the begin step out, and put some other code in.
Rather, I should simply set a priority value that changes with each event.

*/

//	PQevents is Created in EventOrder now, and destroyed at the end of it.

// Stores the event arrays themselves, at a key with a string name.
MAPeventorder = ds_map_create();
//	Stores the string data of the matching event arrays, at a key identical to its paired array in MAPeventorder.
MAPeventdata = ds_map_create();

//ds_priority_add(Qevents, stuff, 0)
#endregion
#region	Event Variables.

event_default = 4;
event_time = 0;
event_timer_update = 0;
event_timer = 0;
// This will be the value I access and change with EventStep.
// It is tracked and stored as events change, but does not actually change the state.
event_on_deck	= noone;

event_last	= event_on_deck;

// This contains the value which tells the StateMachine what to change to.
// When I change "current_state", this value is updated and is responsible for the stateshift.

//event_batting = current_event;

// This is a string used for more easily written/read code. 
// It would perhaps be better served by a CONSTANT set to a script?
name_of_state = "Unknown";


// Since DT is being used, it is necessary to have more than 1 timer variable for
// determining the timer, ie - a start, an (optional) end, and the timer itself.
state_timer_start_at = 0;
state_timer = state_timer_start_at

// This is the variable that will tell the state engine if the current_state is different from
// the previous state. If true, it will perform whatever you want it to just one time, like:
// if state_change { SpriteArray[Direction, Action]}
event_order_changed = false;
event_list_use_index = "Standard";

// Array variable that stores a value ONLY FOR THE DURATION OF THE STATE, and not used by any
// other states.
event_var[0] = 0; 

// If this is true, then the state can be interupted by a given set of actions.
//state_can_interrupt=true;
#endregion


#region	Parent Variable Setting

	#region	Event Data
		
		// Current Event and Event Last setting.
		current_event = EV[BEGINSTEP];
		event_last = EV[BEGINSTEP];
		
		// Event Timers.
		event_timer = 0;
		event_timer_update = 0;
		event_time = 0;
		
		// Event Strings.
		event_string = "Create";
		event_last_string = "none"
		event_next = "Begin Step";

	#endregion
	
	#region	Console Writing and Drawing to Screen.
	// Used to write events performed in a single step to the console.
	console_events = false;
	kconsole = keyboard_check(_F10);


	// Set to true for drawing to screen and false for not.
	eventdata = false;
	#endregion
	
#endregion

#region	Event_Data Array.

	#region	Event Data Array and Update
		Event_Data[0] = "Last Event: " + string(event_last_string);
		Event_Data[1] = "Current Event " + event_string;
		Event_Data[2] = "Next Event " + event_next;
		Event_Data[3] = "Event Timer " + string(event_timer);
		Event_Data[4] = "Enum Current Event: " + string(current_event);
		Event_Data[5] = "Enum Last Event: " + string(event_last);
	#endregion

#endregion








