///	@func			EvenFunctions()

///	@desc			Any Key press or inputs for general control of the event system.

kconsole = keyboard_check_pressed(_F10);

// Switching between console write.
if kconsole
	{console_events = true;}
else 
	{console_events = false;}
		
// Write to console.
if console_events	ConsoleWriteEvents();
