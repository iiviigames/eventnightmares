///	@func								EventEnter(event_number)

///	@arg	{real}	event_number		next event to execute

///	@desc								The script to execute during a given event.


#region	Changing This Now
//show_debug_message("current event: " + string(current_event))

//if argument_count < 1
//	{var _ce = event_default;}
//else
//{var _ce = argument[0];}
#endregion



if event_type == ev_step and event_number == ev_step_normal
{
	current_event = event_default;
}
else
{
	var _eventnumber = argument[0];
	_eventnumber = current_event;
}
// Enter the next event in line unless current_event has been set to 4 (_Event.Complete_)

if current_event != EV[COMPLETE]	// As long as the custom event aren't complete...
{
	event_user(current_event);		// Enter the next event.
}

// The highest value has been reached, so set the event to the first one in the order for next step.
else
{
	ev = 0;							// Reset to the first or default event.
	current_event = ev;				// Set the current_event to default_event for the NEXT game step.
	exit;								// And continue to draw/draw gui for the CURRENT game step.
}
