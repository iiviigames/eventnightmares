///	@func			EventStep()

///	@desc			This is called in the REGULAR STEP event, and will allow for setting custom events with EventExecute()
///					Initial event order is [BEGIN STEP, STEP, COLLISION, END STEP]


//	Event Variables Updating.
EventVariables();

//	Write Events to Console per Step.
EventFunctions();

// Enter the current event
EventEnter(Current_Event);


