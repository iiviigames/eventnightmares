///	@func							EventSetOrder(map_key)
///	@arg	{string}	map_key	the key in MAPeventorder to change to	
///	@desc							Changes the event order to a different one stored in the map

var _switchto = argument0;

// Prevent Changing or attempting to change to an order that hasn't been created.
if !ds_map_exists(MAPeventorder, _switchto) or !ds_map_exists(MAPeventdata, _switchto)
{
	show_error("Invalid event order provided, staying on current order.", false);
}
else
{
	
	// Set the default event.
	EventDefault(order_key)
	
	// Don't forget to reset the event order changed to false!
	event_order_changed = false;
	
	#region	Debugging.
	
	SDO();
	show_debug_message("MAPeventorder size: " + string(ds_map_size(MAPeventorder)));
	show_debug_message("MAPeventdata size: " + string(ds_map_size(MAPeventdata)));
	
	for (i = 0; i < array_length_1d(Current_Event); i++)
	{
		SD(Current_Event[i]);
	}
	#endregion
}