///	@func								EventDebugs()

///	@desc								Sets the variables for a given event

// Notify in console what object and what event this came from.
//SDO();

#region	Set all the bullshit debgs.
// Timers.
event_time = event_timer_update;
event_timer_update = 0;
event_timer_update += TimerSet(F)
var _timeprev = event_timer;

// Begin Step
if event_number == ev_user0
{
	//SD("User 0")
	event_next = "Step";
	event_string = "Begin Step";
	event_time = _timeprev;
	//event_last = EV[EVB];
	event_last_string = "Begin Step";
}
// Step
else if event_number == ev_user1
{
	event_next = "End Step";
	event_string =  "Step";
	event_time = _timeprev;
	//event_last = EV[EVS];
	event_last_string =  "Step";
}
// End Step
else if event_number == ev_user2
{
	event_next = "Collision";
	event_string =  "End Step";
	event_time = _timeprev;
	//event_last = EV[EVE];
	event_last_string = "End Step";

}
// Collision
else if event_number == ev_user3
{
	event_next = "Draw, then Begin Step again";
	event_string =  "Collision";
	event_time = _timeprev;
	//event_last = EV[EVC];
	event_last_string = "Collision";
}
else
	{exit;}
	
#endregion