///	@func								EventDataUpdate(*return_array)

///	@arg	{bool}	return_array		If true the array Event_Data will be returned.

///	@desc								Updates the array Event_Data to corresponding variables.

Event_Data[0] = "Last Event: " + string(event_last_string);
Event_Data[1] = "Current Event " + event_string;
Event_Data[2] = "Next Event " + event_next;
Event_Data[3] = "Event Timer " + string(event_timer);
Event_Data[4] = "Enum Current Event: " + string(current_event);
Event_Data[5] = "Enum Last Event: " + string(event_last);


if argument_count > 0
{
	var _bool = argument[0];
	
	if _bool
		{return Event_Data;}
	else	
		{exit;}
}
else
	{exit;}