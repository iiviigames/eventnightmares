///	@func			EvenFunctions()

///	@desc			Any Key press or inputs for general control of the event system.

#region	Set Data Variables for this event.
// 
EventDebugs();
#endregion

#region	Show object name and event origin.

kconsole = keyboard_check_pressed(_F10);
console_events = false;
// If the above key is pressed, all events the script is called in for this frame will print out their origin.
if kconsole
	{console_events = true;}
else 
	{console_events = false;}
		
// Write to console.
//if console_events	SDO();
#endregion
#region	Show the event data if the corresponding key is pressed.
if eventdata 
	{EventDataUpdate();}
#endregion

#region	Changing event orders (temp)

if keyboard_check_pressed(vk_pause)
{	
	event_order_changed = true;
	if order_set == "Standard"
	{
		order_key = "CollideBeforeEnd";
	}
	else if order_set == "CollideBeforeEnd"
	{
		order_key = "Standard"
	}
}
#endregion	