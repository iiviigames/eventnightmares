///	@func			EventStep()

///	@desc			This is called in the REGULAR STEP event, and will allow for setting custom events with EventExecute()
///					Standard event order is [Begin Step, Step, End Step, Collision, Done]


//	Write Events to Console per Step.
EventFunctions();

// Enter the current event
EventEnter(Current_Event);


