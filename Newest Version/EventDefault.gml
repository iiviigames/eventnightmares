///	@func										EventDefault(order, change_default)
///	@arg	{string}	order_key				the string containing the desired starting order, stored in the MAP with EventSetOrder
///	@arg	{string}	change_default		if a value is provided, this will be used as the event_ddefault value in place of the EP1 value.
///	@desc										Sets the event_default variable

var _order = argument[0];

// Set the desired event order to the Current_Event array.
Current_Event = ds_map_find_value(MAPeventorder, _order);
Current_Event_Data = ds_map_find_value(MAPeventdata, _order);

// Used for iterating through the event array.
ev = Current_Event[0];							// The highest priority event, which is always the first.
ev_max = array_length_1d(Current_Event) - 1 ;	// The number of total events in the new order.

// Set the changing order value.
order_set = _order;
//show_order = false;

SDO();
show_debug_message("MAPeventorder size: " + string(ds_map_size(MAPeventorder)));
show_debug_message("MAPeventdata size: " + string(ds_map_size(MAPeventdata)));


if argument_count == 2
{
	var _default = argument[1];
	event_default = _default;
	current_event = _default;
}
else
{
	event_default = ev;
	current_event = event_default;
}


