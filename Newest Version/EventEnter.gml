///	@func								EventEnter(event)

//		@arg	{real}	event_number		next event to execute

///	@desc								The script to execute during a given event.

// This runs only in the "real" step event, to ensure that the first event in line gets run normally, and not added to or skipped.
if event_type == ev_step and event_number == ev_step_normal
{
	current_event = event_default;
}
else
{	
	var _eventnumber = argument[0];
	_eventnumber = current_event;
}
// Enter the next event in line unless current_event has been set to 4 (_Event.Complete_)

if current_event != EV[EVD]			// As long as the custom event aren't complete...
{
	event_user(current_event);		// Enter the next event. 
}		

// The highest value has been reached, so set the event to the first one in the order for next step.	
else									
{
	ev = 0;							// Reset to the first or default event.
	current_event = ev;				// Set the current_event to default_event for the NEXT game step.
	exit;								// And continue to draw/draw gui for the CURRENT game step.
}										
	