///	@func			EventCustom()

///	@desc			Creates Event Data Structures and accompanying variables.
///					2 dsMAPS	MAPeventorder & MAPeventdata

#region	Create dsMAPS for the Object's Events.

// Stores the event arrays themselves, at a key with a string name.
MAPeventorder = ds_map_create();
//	Stores the string data of the matching event arrays, at a key identical to its paired array in MAPeventorder.
MAPeventdata = ds_map_create();

#endregion

#region	Event Variables.

// Essential Event Vairables.
event_default = 4;						// This is the same as EV[COMPLETE] or _Event.Complete_
ev = 0;									// The iterator value, used to proceed through the event order.
ev_max = event_default;					// Length of Current_Event, when it becomes an array of events.
current_event = ev;						// The value that is used by event_user() to actually enter the event.
//Current_Event = [];						// The array which contains a given event order, iterated through with "ev". ex. Current_Event[ev]

// Changing Event Order Variables.
order_key = "Standard";					// Will contain the value of whatever the first event order was stored with. This notifys the order_set value of a change in order.
order_set = order_key;					// This is the variable that actually changes the event order when updated.
show_order = false;						// Boolean determining if the event order should be drawn to screen for debug purposes.
event_order_changed = false;			//	Used to track when an event order is different that in the previous iteration/step.
event_list_use_index = order_key;		// The mandatory value for the first event order.


// Timers.
event_time = 0;
event_timer_update = 0;
event_timer = 0;

// Event Strings.
event_string = "Create";
event_last_string = "none"
event_next = "none";

// Array variable that stores a value ONLY FOR THE DURATION OF THE EVENT ORDER, and not used by any
// other states.
event_var[0] = 0; 

// If this is true, then the event can be interupted by a given set of actions.
event_interrupt = true;

#endregion


	
	#region	Console Writing and Drawing to Screen.
	// Used to write events performed in a single step to the console.
	console_events = false;
	kconsole = false;


	// Set to true for drawing to screen and false for not.
	eventdata = false;
	#endregion
	


#region	Event_Data Array.
/*
	#region	Event Data Array and Update
		Event_Data[0] = "Last Event: " + string(event_last_string);
		Event_Data[1] = "Current Event " + event_string;
		Event_Data[2] = "Next Event " + event_next;
		Event_Data[3] = "Event Timer " + string(event_timer);
		Event_Data[4] = "Enum Current Event: " + string(current_event);
		Event_Data[5] = "Enum Last Event: " + string(event_last);
	#endregion
*/
#endregion








