///	@desc									EventOrder(bsp, sp, esp, cp, dp, key, return_key)

///	@arg	{real}		bsp				begin step pri
///	@arg	{real}		sp					step pri
///	@arg	{real}		esp				end step pri
///	@arg	{real}		cp					collision pri
///	@arg	{real}		dp					complete pri
///	@arg	{string}	new_list			string to use as a key in the new map values
///	@arg	{bool}		return_key			if true the string used as a key for the map values will be returned

///	@ret	{string}	map_key			if return_key is true, this will return the key to use for accessing or setting the current event order

///	@desc									Creates a new value and priority in the ds PQevents			
///											Creates an array containing the new event order.
///											The lower an event's priority value, the sooner it will execute.

var _beginpri = argument[0];
var _steppri = argument[1];
var _endpri = argument[2];
var _colpri = argument[3];
var _compri = argument[4];
var _liststring = argument[5];
var _returnkey = argument[6];

// Create the priority queue
PQevents = ds_priority_create();

//	Check that priorities are no less than -1 and no greater than 4
if (_beginpri < -1 or _steppri < -1 or  _endpri < -1 or _colpri < -1 or _compri < -1) or 
(_beginpri > 4 or _steppri > 4 or  _endpri > 4 or _colpri > 4 or _compri > 4)
{
	show_error("Event priority out of range. Values must be set between -1 and 4.", false);
	exit;
}
else
{

	#region	Priority array to loop through and enter into the priority queue.
	var _priorities = [_beginpri, _steppri , _endpri, _colpri, _compri];
	
	// Strings containing the name of the event, matched to the corresponding event macro value.
	var _eventstrings = [EVBst, EVSst, EVEst, EVCst, EVDst];
	
	// Sets up a local which will become an instance array containing the string and priority value of the new event order.
	var _eventlist;
	// Only counts up when the _eventlist is added to.
	var _eventlistcount = 0;
	
	//	Loop through the array of priorities. 
	for (var i = 0; i < 5; i++)
	{
		//	First, skip over any events that have been set to -1.
		if _priorities[i] == -1
			{continue;}
		//	Then, add the remaining events to the current event order priority.
		else
		{
			ds_priority_add(PQevents, EV[i], _priorities[i]);
			
			// Create the event list for debugging.
			_eventlist [_eventlistcount] = _eventstrings[i] + string(_priorities[i]);

			//	Increase the _eventlistcount.
			_eventlistcount += 1;
		}
	}
	#endregion
}

#region	Create the arrays that will be contain the event order and string event order.

// Variable for looping through the priority queue.
var _pqsize = ds_priority_size(PQevents);

// Array which contains the event order that will be returned at the end of this script.
var _Events = array_create(_pqsize);

// Instance array with a handle of whatever was passed and stored in _liststring.
//variable_instance_set(self, _liststring, _eventlist);	//	(It has to be done this way, as we already return another array in this script.)

#endregion

#region	Loop through the priority queue, finding the lowest priority value at each iteration, removing it, and then storing it in the _Events array.
for (var i = 0; i < _pqsize ; i++)
{
	// Grab the lowest value in the pq and set it to a local.
	var _event = ds_priority_find_min(PQevents);
		
	// Delete the lowest value i pqevents, which was just set to the local _event.
	ds_priority_delete_min(PQevents);
		
	// Show a debug message of the _event local.
	//SD("Event number stored in _list at position " + string(i) + " : " + string(_event));
		
	// Set the current iteration value to the same position in the _list array.
	_Events[i] = _event;
	
	// Show a debug message of the above line.
	//SD(_Events[i]);
		
	// If the priority queue is not yet empty, iterate again.
	if !ds_priority_empty(PQevents)
		{continue;}
	// If it is empty, clear it, destroy it, and set it to -1.
	else
	{
		ds_priority_clear(PQevents);
		ds_priority_destroy(PQevents);
		PQevents = -1;
	}
		
}
#endregion

//	Return the _Events array for use with current_event.
//return _Events;

// Add the new order to the maps.
EventMapsAdd(_Events, _eventlist, _liststring, _returnkey)
if _returnkey
{
	return _liststring
}
else
{
	exit;
}

#region	Notes
/*
#region	Locals for Arguments

var _statename, _statescript;
_statename = argument[0];
_statescript = argument[1];

#endregion
