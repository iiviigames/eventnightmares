///	@desc		Prop Initialization


#region Init the custom event scripts.
EventCustom();

// Initialize any event orders to store for use.
order_key = EventOrder(0, 1, 2, 3, 4, "Standard", true);
EventOrder(-1, 0, 2, 1, 3, "CollideBeforeEnd", false);

// Set the default event.
EventDefault(order_key)

#region	Debugging.

for (i = 0; i < array_length_1d(Current_Event); i++)
{
	//SD("Current Event " + string(i) + " : " + string(Current_Event[i]));
	//SD(	Event_List[i]	);
	//SD(	SeEsCoDo[i]	);
	SD(Current_Event[i]);
}
#endregion

#endregion