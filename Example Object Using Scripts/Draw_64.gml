///@desc	Draw information
if keyboard_check_pressed(vk_f10)
	{SD("Draw GUI Event.");}




#region	Draw Current Event Data

if keyboard_check_pressed(vk_insert)
{
	show_order = !show_order;
}


if show_order
{
		//GUICY and guiCX are globals that track the GUI positions
	draw_set_color(c_green);
	draw_set_font(f_text);
	DrawTextArray(guiCY, guiX + 20, 0, 20, Current_Event_Data);
	draw_set_color(c_white);
	draw_set_font(f_pixel);
}
#endregion
