///	@desc		Begin Step
///				Execute Necessary Code, then switch to next event in the current order.


// Perform any data updates. (F10 presses and such.)
EventFunctions();
	
//	Set next Current_Event.	(SCRIPTS GO HERE)
EventCurrent(Current_Event);
	
	
//	Check for custom event completion or enter next custom event.
EventEnter(current_event);

exit;

