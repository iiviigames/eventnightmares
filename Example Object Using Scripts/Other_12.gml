///	@desc		End Step
///				Execute Necessary Code, then switch to Collision event.

// Write current event string to console.
EventFunctions();

//	Set next Current_Event.	(SCRIPTS GO HERE)
script_execute(script_update);
EventCurrent(Current_Event);

//	Check for custom event completion or enter next custom event.
EventEnter(current_event);

exit;


