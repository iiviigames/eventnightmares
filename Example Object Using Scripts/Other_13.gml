///	@desc		Collision
///				Execute Necessary Code, then switch to Collision event.


// Perform any data updates. (F10 presses and such.)
EventFunctions();

//	Set next Current_Event.	(SCRIPTS GO HERE)
script_execute(script_whatever);
//	This is the last possible event most times.
EventCurrent(Current_Event);

//	Check for custom event completion or enter next custom event.
EventEnter(current_event);

exit;	// Safety exit.