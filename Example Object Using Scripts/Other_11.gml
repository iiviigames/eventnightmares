///	@desc		Step
///				Execute Necessary Code, then switch to End Step event.


// Perform any data updates. (F10 presses and such.)
EventFunctions();
	
//	Set next Current_Event.	(SCRIPTS GO HERE)
script_execute(FixPropEventStep);
//(Normally, the EventCurrent(Current_Event) would go here, but it's being called from within the above script!)

//	Check for custom event completion or enter next custom event.
EventEnter(current_event);

exit;