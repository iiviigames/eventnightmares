///	@func				SDO()
///	@desc				Shows the origin of the debug message, to prevent being unable to remember where it's coming from later.

var _et = event_type;
var _en = event_number;
var _as = async_load;
var _eventcall;

#region	Switch for getting event type/event number if necessary.
switch (_et)
{
	case ev_create:			
		_eventcall	=	"Create";
		break;
		
	case ev_draw:
		_eventcall	=	"Draw";
		break;
		
	case ev_gui:
		_eventcall	=	"Draw GUI";
		break;
		
	case ev_step:
	{
		switch(_en)
		{
				case ev_step_normal:
					_eventcall	=	"Step";
					break;
						
				case ev_step_begin:
					_eventcall	=	"Begin Step";
					break;
						
				case ev_step_end:
					_eventcall	=	"End Step";
					break;
		}
				
		break;
	}
		
		
	
	
	case ev_other:
	{
		switch(_en)
		{
				case ev_user0:
					_eventcall	=	"(@user0) Custom Begin Step ";
					break;
						
				case ev_user1:
					_eventcall	=	"(@user1) Custom Step ";
					break;
						
				case ev_user2:
					_eventcall	=	"(@user2) Custom End Step ";
					break;
						
				case ev_user3:
					_eventcall	=	"(@user3) Custom Collision ";
					break;
		}
				
		break;
		

			
	}
	
	default:
		_eventcall	=	"Not implemented; Update SDO()";
}

#endregion

// If the event is not asynchronous...
if _as = -1
{
	show_debug_message("");
	show_debug_message("Debug Message Called From: " );
	show_debug_message(object_get_name(object_index) + " in the " + _eventcall + " Event.");
	show_debug_message("-------------");
}
// If the event IS asynchronous...
else
{
	show_debug_message("");
	show_debug_message("Debug Message Called From: " );
	show_debug_message(object_get_name(object_index) + " in the " + string_upper(async_load[? "event_type"]) + " Event.");
	show_debug_message("-------------");
}