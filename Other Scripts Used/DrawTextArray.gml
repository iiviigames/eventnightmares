///	@function					DrawTextArray(x, y, x_offset, y_offset, Array_Name)

///	@arg	{real}  			x_start    		X Start drawing from 
///	@arg	{real}  			y_start    		Y Start drawing from
///	@arg	{real}  			x_offset   		Horizontal shift to draw subseq. entries
///	@arg	{real}  			y_offset   		Vertical shift to draw subseq. entries
///	@arg	{handle}			array_index	    Name of the array to draw.

///	@description				Takes the values of an array and draws them at an even interval down the screen.

var _xx = argument[0];
var _yy = argument[1];
var _xincrease = argument[2];
var _yincrease = argument[3];
var _array = argument[4];

var _arraylength = array_length_1d(_array);



var ii;
for (ii = 0; ii < _arraylength; ii++)
{
   //Draw the array of text.
    draw_text_transformed(_xx + (ii * _xincrease), _yy + (ii * _yincrease),  + _array[ii], 1, 1, 0);
}
