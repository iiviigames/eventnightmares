/// @function				SD(string1, string2, etc.)
/// @desc    				Shows debug messages of any amount
var _args = argument_count


for (var i = 0; i < _args; i++)
{
    
    show_debug_message(string(argument[i]));
}


